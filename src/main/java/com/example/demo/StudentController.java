package com.example.demo;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.in.StudentRepo;


@org.springframework.stereotype.Controller
public class StudentController {
	
	@Autowired
	
	StudentRepo repo;

	@RequestMapping("/home")
	public ModelAndView home()
	{
		System.out.println("Controller invoked");
		return new ModelAndView("index");
		//return "s1.html";

		//return "welcome to Spring Boot Application";
		
	
		
		
	}
	@RequestMapping("/addStudent")
	public ModelAndView addStudent(@Valid Student std)
	{
		repo.save(std);
		return new ModelAndView("show");
	}
	
	
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public ModelAndView login(Model model,String error,String Logout)
	{
		if(error!=null)
			model.addAttribute("your usename and password is invalid");
	//	if(logout!=null)
	//		model.addAttribute("logged out successfully");

		return new ModelAndView("login");
		//return "s1.html";

		//return "welcome to Spring Boot Application";
		
	
		
		
	}

	

	@RequestMapping("/welcome")
	public ModelAndView welcome()
	{
	
		return new ModelAndView("welcome");
		//return "s1.html";

		//return "welcome to Spring Boot Application";
		
	
		
		
	}

	
	//@RequestMapping("/getStudent")
	//public ModelAndView getStudent(@RequestParam int id)
	//{
		//ModelAndView mv= new ModelAndView("show");

	//Student std=repo.findById(id).orElse(new Student());
	//return mv;

//	return mv();
	

	//}
}
